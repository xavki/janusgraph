%title: JanusGraph
%author: xavki


# JANUSGRAPH : Introduction, c'est quoi ??


<br>

Graphe :

		* Un modèle est orienté graphe ?? > entités reliées par des arcs
				nodes/relations ou vertices/edges

<br>

		* en opposition au relationnel tabulaire

<br>

		* et aux modèles standards nosql : key/value, documents, colonnes...

<br>

		* le graphe permet de représenter des relations multiples et complexes
				sens de la relation
				pointeurs variants dans les relations...

<br>

		* performances parfois complexes car dépendant de la structure du graphe

<br>

		* une requête consiste à parcourir le graphe (plus ou moins en profondeur)

<br>

		* définir ou non le schéma (flexibilité total)

---------------------------------------------------------------------------------------

# JANUSGRAPH : Introduction, c'est quoi ??

<br>

Exemples : 

		* réseaux sociaux (relations entre les membres et les entités différentes)

		* réseaux informatiques

		* réseaux de transports

---------------------------------------------------------------------------------------

# JANUSGRAPH : Introduction, c'est quoi ??

<br>

JanusGraph

		* moteur de base de données opensource orienté graphe

		* Attention différent de GraphQL : langage et runtime pour API (issu de Facebook)

		* grandes quantité de données modélisée en graphe (multiples connecteurs)

		* différents mode d'architecture frontend/backend
		

---------------------------------------------------------------------------------------

# JANUSGRAPH : Introduction, c'est quoi ??

<br>

		* bonne scalabilité car sur système distribué
		
<br>

		* notamment avec différents backends :
				Cassandra (Scylla)
				Hadoop
				BigTable
				BerkeleyDB

<br>

		* les possibilités d'indexation : Solr, Lucene Elasticsearch

<br>

		* possibilité de passer de OLTP / OLAP
		
<br>

		* basé sur le framework Tinkerpop / langage Gremlin

<br>

		* Langages couramment supportés : Java, Python, Javascript, Scala

---------------------------------------------------------------------------------------

# JANUSGRAPH : Introduction, c'est quoi ??

<br>


Tinkerpop

	* Fondation Apache : https://tinkerpop.apache.org/

	* framework opensource pour moteur orienté graphe

	* couche d'abstraction et outils pour exploiter les données

	* langage nommé Gremlin

	* connecteurs à des moteurs de données

	* moteurs les plus connus : Janusgraph, Neo4J, amazon Neptune, ArangoDB, OrientDB, TigerGraph

---------------------------------------------------------------------------------------

# JANUSGRAPH : Introduction, c'est quoi ??

<br>

Gremlin : 

	* langage "simple" pour requêter/traverser un graph

	* lecture/écriture des vertex et edges

	* Mode d'utilisation :

			* CLI
		
			* client applicatif

			* API gremlin

	* utilise une syntaxe de type pipeline (enchainement de tâches) et plus adapté aux graphes
